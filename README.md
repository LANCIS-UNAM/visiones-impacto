# Visiones de sostenibilidad y sus impactos

## Objetivo general

Creación de una plataforma para la evaluación del impacto de proyectos mexicanos 
en torno a sostenibilidad respecto de la definición del problema según posturas
geopolíticamente diversas.

[Módulo de Perfilamiento](https://gitlab.com/LANCIS-UNAM/visiones-impacto/-/wikis/M%C3%B3dulo-de-Perfilamiento)

Hemos creado un modelo de datos para registrar los diferentes aspectos
del perfil político de cualquier participante. Consiste de tablas para
documentación acerca de enfoques de proyectos, vinculados a su
bibliografía relevante. Relacionada a esta tabla de enfoques creamos
una tabla de reactivos que conformarán el instrumento. Finalmente
creamos la tabla y los formularios necesarios para aplicar un
cuestionario que registra el nivel en que el participante se
identifica con los diferentes enfoques. Todo lo antes descrito está
codificado en el marco de desarrollo web Django, que se programa con
Python, HTML5 y Tailwind. El prototipo permite la gestión de todos los
datos relacionados con la creación y mantenimiento de la batería de
reactivos y las respuestas de los participantes.


[Módulo de ToC](https://gitlab.com/LANCIS-UNAM/visiones-impacto/-/wikis/M%C3%B3dulo-de-ToC)

[Módulo de Crowdsourced Feedback](https://gitlab.com/LANCIS-UNAM/visiones-impacto/-/wikis/M%C3%B3dulo-de-Crowdsourced-Feedback)



