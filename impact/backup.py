from pathlib import Path
import subprocess
from datetime import datetime


dbpath = Path('/var/www/visiones-impacto/impact/db.sqlite3')
remote_path = 'rgh@patung.lancis.ecologia.unam.mx:./impact_bak'


bakdir = Path.home().joinpath('backup/impacto')
bakdir.mkdir(parents=True, exist_ok=True)

bakpath=bakdir.joinpath(datetime.date(datetime.now()).isoformat() + '.sqlite3')

subprocess.call(["cp",
                 dbpath,
                 bakpath])

remote_mnt = Path.home().joinpath('backup/mnt')
remote_mnt.mkdir(parents=True, exist_ok=True)

subprocess.call(["sshfs",
                 remote_path,
                 remote_mnt])

subprocess.call(["cp",
                 bakpath,
                 remote_mnt])


subprocess.call(["fusermount",
                 '-u',
                 remote_mnt])
