"""impact URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from stance import views
from toc import views as toc_views

from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'approaches', views.ApproachViewSet)
router.register(r'orientations', views.NormativeOrientationViewSet)
router.register(r'projections',
                views.ProjectionViewSet,
                basename='projection')



urlpatterns = [
    path('api/', include(router.urls)),
    path('admin/', admin.site.urls),
    path('accounts/', include('allauth.urls')),
    path(
        r'reference-autocomplete/',
        views.ReferenceAutocomplete.as_view(),
        name='reference-autocomplete'),
    path(
        r'approach-autocomplete/',
        views.ApproachAutocomplete.as_view(),
        name='approach-autocomplete'),

    path('assay/',
         views.AssayView.as_view(),
         name='assay'),

    path('approaches/',
         views.ApproachListView.as_view(),
         name='approach_list'),

    path('approaches/pdf/',
         views.buildPDF,
         name='approachespdf'),

    path('approaches/odt/',
         views.buildODT,
         name='approachesodt'),

    path('toc/digraph/',
         toc_views.toc_digraph_view,
         name='toc_digraph'),

    path('stance/pc/',
         views.stance_pc,
         name='stance_pc'),

    path('stance/projection/<int:ox_id>/<int:oy_id>/',
         views.ProjectionView.as_view(),
         name='projection'),
    
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
