# Generated by Django 3.2.7 on 2022-11-25 18:55

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('stance', '0011_auto_20221125_1843'),
    ]

    operations = [
        migrations.AddField(
            model_name='approach',
            name='author',
            field=models.ForeignKey(default=1, help_text='author', on_delete=django.db.models.deletion.CASCADE, to='auth.user', verbose_name='author'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='approach',
            name='status',
            field=models.CharField(choices=[('p', 'p'), ('v', 'v'), ('r', 'r')], default='p', max_length=2),
        ),
        migrations.AlterField(
            model_name='approach',
            name='discourse',
            field=models.TextField(default='', help_text='socio-political discourse', verbose_name='socio-political discourse'),
        ),
    ]
