# Generated by Django 3.2.7 on 2022-11-25 18:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('stance', '0010_auto_20221013_2106'),
    ]

    operations = [
        migrations.AddField(
            model_name='approach',
            name='discourse',
            field=models.TextField(default='', help_text='socio-political discourse', verbose_name='description'),
        ),
        migrations.AddField(
            model_name='approach',
            name='strong',
            field=models.BooleanField(default=False, help_text='Is this approach soft or strong?'),
        ),
        migrations.AddField(
            model_name='reference',
            name='year',
            field=models.IntegerField(default=1900, help_text='year of publication'),
        ),
        migrations.AlterField(
            model_name='reference',
            name='citation',
            field=models.TextField(help_text='citation, APA style', verbose_name='citation'),
        ),
        migrations.AlterField(
            model_name='reference',
            name='url',
            field=models.URLField(blank=True, help_text='preferably DOI but others are fine if not available', null=True),
        ),
    ]
