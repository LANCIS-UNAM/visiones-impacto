from django.contrib.auth.models import User, Group
from stance.models import Approach, NormativeOrientation, Projection
from rest_framework import serializers


class ApproachSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Approach
        exclude = ['bibliography', ]

class NormativeOrientationSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:    
        model = NormativeOrientation
        exclude = ['bibliography', ]

class ProjectionSerializser(serializers.HyperlinkedModelSerializer):

    author = serializers.HiddenField(
        default=serializers.CurrentUserDefault()
    )
    
    class Meta:    
        model = Projection
        fields = '__all__'

        
class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'groups']

