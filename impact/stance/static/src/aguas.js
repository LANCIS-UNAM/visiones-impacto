
// Get a reference to the canvas object
window.canvas = document.getElementById('canvas');
// Create an empty project and a view for the canvas:
paper.setup(window.canvas);

var all_approaches = [];
var remaining = [];
var draggable_list = document.getElementById('draggable-list');
var listItems = [];
const xhr = new XMLHttpRequest();
xhr.open("GET", "/api/approaches/");
xhr.send();
xhr.responseType = "json";
xhr.onload = () => {
    if (xhr.readyState == 4 && xhr.status == 200) {
	all_approaches = xhr.response;
	remaining = [...all_approaches];
	draggable_list = document.getElementById("draggable-list");
    pon_leyenda();
    } else {
	console.log(`Error: ${xhr.status}`);
    }
};




var all_projections = [];

const xhr0 = new XMLHttpRequest();
xhr0.open("GET", "/api/projections/");
xhr0.send();
xhr0.responseType = "json";
xhr0.onload = () => {
    if (xhr0.readyState == 4 && xhr0.status == 200) {
	all_projections = xhr0.response;
    } else {
	console.log(`Error: ${xhr0.status}`);
    }
};

var ox = {};
const xhr1 = new XMLHttpRequest();
xhr1.open("GET", "/api/orientations/"+ox_id+"/");
xhr1.send();
xhr1.responseType = "json";
xhr1.onload = () => {
      if (xhr1.readyState == 4 && xhr1.status == 200) {
	  ox = xhr1.response;
      ox1.content = ox.pole1;
      ox2.content = ox.pole2;
      } else {
	  console.log(`Error: ${xhr1.status}`);
      }
  };

  var oy = {};
  const xhr2 = new XMLHttpRequest();
  xhr2.open("GET", "/api/orientations/"+oy_id+"/");
  xhr2.send();
  xhr2.responseType = "json";
  xhr2.onload = () => {
      if (xhr2.readyState == 4 && xhr2.status == 200) {
	  oy = xhr2.response;
      oy1.content = oy.pole1;
      oy2.content = oy.pole2;
       } else {
	   console.log(`Error: ${xhr2.status}`);
       }
  };

  const csrftoken = Cookies.get('csrftoken');
  ya = function(approach, orientation){
    this_projection = all_projections.filter(proj => proj.orientation.url == orientation.url && proj.approach.url == approach.url);
    console.log(this_projection);
    if (this_projection.length == 0){
        return false;
    }else{
        return this_projection[0];
    }
    
  }
  post_projection = function(approach, orientation, magnitude) {
    const xh = new XMLHttpRequest();
    this_proj = ya(approach, orientation);
      if (this_proj){
        xh.open("PUT", this_proj.url);
      }else{
        xh.open("POST", "/api/projections/");
      }
      
      
      
      xh.setRequestHeader("Accept", "application/json");
      xh.setRequestHeader("Content-Type", "application/json");
      xh.setRequestHeader('X-CSRFToken', csrftoken);
      xh.responseType = "json";
//      xhr.onload = () => console.log("respuesta", xhr.response);

      xh.send(JSON.stringify({ "magnitude": magnitude,
				"approach": approach.url,
				"orientation": orientation.url}));
    xh.responseType = "json";
    xh.onload = () => {
            if (xh.readyState == 4 && (xh.status == 201 || xh.status == 200)) {
                if (!this_proj){
                    all_projections.push(xh.response);
                }else{
                    console.log("kk");
                }
            } else {
                console.log(`Error: ${xh.status}`);
            }
        };


      
  };

  fill_remaining = function(){
    console.log("empecé el for de remaining");
    remaining.forEach(function (item, index) {
	var listItem = document.createElement("li");
	listItem.setAttribute('data-index', index);
	indexp1 = index + 1;
	listItem.innerHTML = '<span class="number">'+ indexp1 + '</span><div class="draggable" draggable="true"><p class="aproach">' + item.title + '</p></div>';

	// if (index == 9){
	//   listItem.innerHTML = listItem.innerHTML + '<hr />';
	// }
	listItems.push(listItem);
	draggable_list.appendChild(listItem);

	// label.setAttribute("title", item.description);

    });
    console.log("acabé el for de remaining");
    addEventListeners();
  };
  function addEventListeners(){
    const draggables = document.querySelectorAll('.draggable');
    const dragListItems = document.querySelectorAll('.draggable-list li');

    draggables.forEach(draggable => {
      draggable.addEventListener('dragstart', dragStart);
    })

    dragListItems.forEach(item => {
      item.addEventListener('dragover', dragOver);
      item.addEventListener('drop', dragDrop);
      item.addEventListener('dragenter', dragEnter);
      item.addEventListener('dragleave', dragLeave);
    })
  }
  let dragStartIndex;

  function swapItems(fromIndex, toIndex){
    console.log(dragStartIndex, dragEndIndex);
    const itemOne = listItems[fromIndex].querySelector('.draggable');
    fromItem = Object.assign({}, remaining[fromIndex]);
    console.log("quitndo", fromItem.title);
    remaining.splice(fromIndex,1);

    if (fromIndex < toIndex){
      listItems[toIndex].appendChild(itemOne);
      remaining.splice(toIndex, 0, fromItem);
      for (var i = fromIndex; i < toIndex; i++){
	item_i = listItems[i+1].querySelector('.draggable');
	listItems[i].appendChild(item_i);
      }
    }else{
      console.log("agregando", fromItem.title);
      remaining.splice(toIndex, 0, fromItem);
      for (var i = fromIndex; i > toIndex; i--){
	item_i = listItems[i-1].querySelector('.draggable');
	listItems[i].appendChild(item_i);
      }
      listItems[toIndex].appendChild(itemOne);
    }


  }
  function dragStart() {
    //console.log('Event:', 'dragstart');
    dragStartIndex = +this.closest('li').getAttribute('data-index');
    console.log(dragStartIndex);
  }
  function dragOver(e) {
    //console.log('Event:', 'dragOver');
    e.preventDefault();
  }
  function dragDrop() {
    dragEndIndex = +this.getAttribute('data-index');
    swapItems(dragStartIndex, dragEndIndex);
    this.classList.remove('over');
  }
  function dragEnter() {
    //console.log('Event:', 'dragEnter');
    this.classList.add('over');
  }
  function dragLeave() {
    console.log('Event:', 'dragLeave');
    this.classList.remove('over');
  }
  function proj2canvas(approach){
    f_px = all_projections.filter(proj => proj.orientation.url == ox.url && proj.approach.url == approach.url);
    f_py = all_projections.filter(proj => proj.orientation.url == oy.url && proj.approach.url == approach.url);
    canvas_x = 0;
    canvas_y = 0;
    if (f_px.length > 0 && f_py.length > 0){
	console.log(f_px.length,f_py.length);
	px = f_px[0];
	py = f_py[0];
	m_x = parseFloat(px.magnitude);
	m_y = parseFloat(py.magnitude);
	canvas_x = 400*(1.0 + m_x);
	canvas_y = 400*(1.0 - m_y);
	console.log("vengo de",m_x, m_y);
	//m_x -> (x.position.x-400)/400;
	//x -> 400*(m_x + 1)

	//m_y -> (400 - y.position.y)/400;
	//y -> 400*(1 - m_y)
    }
    return canvas_x, canvas_y;
  }




///////////////////////////////////////////// las flechas
function Arrow (mouseDownPoint) {
    this.start = new paper.Point(400, 400);
    this.headLength = 20;
    this.tailLength = 9;
    this.headAngle = 35;
    this.tailAngle = 110
}

Arrow.prototype.draw = function (point, color) {
    var end = point;
    var arrowVec = this.start.subtract(end);
    // parameterize {headLength: 20, tailLength: 6, headAngle: 35, tailAngle: 110}
    // construct the arrow
    var arrowHead = arrowVec.normalize(this.headLength);
    var arrowTail = arrowHead.normalize(this.tailLength);
    var p3 = end;                  // arrow point
    var p2 = end.add(arrowHead.rotate(-this.headAngle));   // leading arrow edge angle
    var p4 = end.add(arrowHead.rotate(this.headAngle));    // ditto, other side
    var p1 = p2.add(arrowTail.rotate(this.tailAngle));     // trailing arrow edge angle
    var p5 = p4.add(arrowTail.rotate(-this.tailAngle));    // ditto
    // specify all but the last segment, closed does that
    this.path = new paper.Path(this.start, p1, p2, p3, p4, p5);
    this.path.closed = true;
    this.path.strokeWidth = 1
    this.path.strokColor = color
    this.path.fillColor = color

    return this.path
}




//////////////////////////////////// las variables

var draggable_list = document.getElementById('draggable-list');



colores10 = ['#1f78b4','#b2df8a','#33a02c','#fb9a99','#e31a1c','#fdbf6f','#ff7f00','#cab2d6','#6a3d9a','#ffff99'];
var currentcolor = colores10[0];
var current_index = 0;
var current;
var leyendaDiv = document.getElementById("approaches_div");

// approaches are requested from REST API
var approaches = [];
var en_cual_vamos = 0;

/////////////////////////////// los ejes de orientaciones normativas 1 y 2
var ejex = new paper.Path.Line(new paper.Point(0, 400), new paper.Point(800, 400));
var f_ejex_1 = new paper.Path.Line(new paper.Point(0, 400), new paper.Point(10, 410));
var f_ejex_2 = new paper.Path.Line(new paper.Point(0, 400), new paper.Point(10, 390));
var f_ejex_3 = new paper.Path.Line(new paper.Point(800, 400), new paper.Point(790, 410));
var f_ejex_4 = new paper.Path.Line(new paper.Point(800, 400), new paper.Point(790, 390));
f_ejex_1.strokeColor = 'black';
f_ejex_2.strokeColor = 'black';
f_ejex_3.strokeColor = 'black';
f_ejex_4.strokeColor = 'black';
ejex.strokeColor = 'black';
var ejey = new paper.Path.Line(new paper.Point(400, 0), new paper.Point(400, 800));
var f_ejey_1 = new paper.Path.Line(new paper.Point(400, 0), new paper.Point(410, 10));
var f_ejey_2 = new paper.Path.Line(new paper.Point(400, 0), new paper.Point(390, 10));
var f_ejey_3 = new paper.Path.Line(new paper.Point(400, 800), new paper.Point(410, 790));
var f_ejey_4 = new paper.Path.Line(new paper.Point(400, 800), new paper.Point(390, 790));
f_ejey_1.strokeColor = 'black';
f_ejey_2.strokeColor = 'black';
f_ejey_3.strokeColor = 'black';
f_ejey_4.strokeColor = 'black';
ejey.strokeColor = 'black';

///////////////////////////////////////// los labels de los ejes
var ox1 = new paper.PointText({
	point: new paper.Point(750,420),
	justification: 'center',
	fontSize: 13,
	fillColor: 'black'
});



var ox2 = new paper.PointText({
	point: new paper.Point(50,420),
	justification: 'center',
	fontSize: 13,
	fillColor: 'black'
});



var oy1 = new paper.PointText({
	point: new paper.Point(390,50),
	justification: 'center',
	fontSize: 13,
	fillColor: 'black',
    rotation: 270
});



var oy2 = new paper.PointText({
	point: new paper.Point(410,750),
	justification: 'center',
	fontSize: 13,
	fillColor: 'black',
    rotation: 90
});


////////////////////////////////// las proyecciones sobre los ejes
var x = new paper.Path.Circle(new paper.Point(400, 400), 3);
x.strokeColor = 'black';
var y = new paper.Path.Circle(new paper.Point(400, 400), 3);
y.strokeColor = 'black';
var textx = new paper.PointText({
	point: new paper.Point(400,420),
	justification: 'center',
	fontSize: 10,
	fillColor: 'black'
});
var texty = new paper.PointText({
	point: new paper.Point(415,400),
	justification: 'center',
	fontSize: 10,
	fillColor: 'black'
});







paper.view.onMouseDown = function (e) {
    approaches.forEach(function (aproach, i) {
	if (aproach.arrow.path){
	    distance = e.point.getDistance(aproach.arrow.path.getNearestPoint(e.point));
	}else{
	    distance = 100;
	}
	if (distance<5){
	    current = aproach.arrow.path;
	    currentcolor = current.strokColor;
	    current_index = i;
	    highlight_aproach(i);
	}
    });
}

paper.view.onMouseDrag = function (e) {
    largo = approaches.length;
    console.log(e.point.x, e.point.y);
    if (current) current.remove()
    x.position.x = e.point.x;
    y.position.y = e.point.y;
    x.position.x = Math.min(x.position.x, 800)
    x.position.x = Math.max(x.position.x, 0)
    y.position.y = Math.min(y.position.y, 800)
    y.position.y = Math.max(y.position.y, 0)
    current = approaches[current_index].arrow.draw(new paper.Point(x.position.x, y.position.y), currentcolor);
    textx.position.x = x.position.x
    textx.content = Math.round((x.position.x-400)/400 * 10) / 10;
    texty.position.y = y.position.y
    texty.content = Math.round((400 - y.position.y)/400 * 10) / 10;
    approaches[current_index].scores.orientacion1 = textx.content;
    approaches[current_index].scores.orientacion2 = texty.content;
}

paper.view.onMouseUp = function (e) {
    approach = approaches[current_index];
    ox_mag = approaches[current_index].scores.orientacion1;
    post_projection(approach,
		    ox,
		    ox_mag)

    oy_mag = approaches[current_index].scores.orientacion2;
    post_projection(approach,
		    oy,
		    oy_mag)

}

highlight_aproach = function (index) {
    approaches.forEach(function (item, indx) {
	cada_boton = document.getElementById("aproach_button_"+ indx);
	cada_boton.style.fontWeight = "500";
	if (item.scores.orientacion1 == "0" && item.scores.orientacion2 == "0"){
	    cada_boton.style.borderColor = "gray";
	    cada_boton.style.borderWidth = "thin";
	}else{
	    cada_boton.style.borderColor = item.color;
	    cada_boton.style.borderWidth = "medium";
	}

	cada_boton.style.marginLeft = "0px";
	cada_boton.style.borderRadius = "4px";
    });
    el_boton = document.getElementById("aproach_button_"+ index);
    el_boton.style.fontWeight = "900";
    el_boton.style.borderColor = currentcolor;
    el_boton.style.borderWidth = "thick";
    el_boton.style.borderRadius = "4px";
    el_boton.style.marginLeft = "20px";
}

activate_aproach = function (aproach, su_index) {
    console.log(ox.url,oy.url,aproach.url);
    console.log("activando", aproach.title);
    current_index = su_index;
    currentcolor = aproach.color;
    if (proj2canvas(aproach)){
	canvas_x, canvas_y = proj2canvas(aproach);
	console.log("pondria la flecha en", canvas_x, canvas_y);
	aproach.arrow.draw(new paper.Point(canvas_x, canvas_y), currentcolor);
    }
    current = aproach.arrow.path;
    highlight_aproach(su_index);
}

quita_flechas = function (){
    approaches.forEach(function (item, index) {
	if (item.arrow.path){
	    item.arrow.path.remove();
	}
    });
}

siguientes_10 = function (){
    quita_flechas();
    // if (all_approaches.length - en_cual_vamos < 10){
    //     approaches = all_approaches.slice(en_cual_vamos);
    // }else{
    //     approaches = all_approaches.slice(en_cual_vamos, en_cual_vamos + 10);
    // }
    approaches = remaining.slice(0, 10);
    remaining = remaining.slice(approaches.length);
    en_cual_vamos = en_cual_vamos + approaches.length;
    console.log("vamos en el ", en_cual_vamos);
    approaches.forEach(function (item, index) {
	item.color = colores10[index];
	item.scores = {orientacion1: 0, orientacion2: 0, orientacion3: 0, orientacion4: 0};
	item.arrow = new Arrow(new paper.Point(405,405),item.color);
    });
    console.log("son",approaches.length,"approaches")
    for (var i = 0; i < 10; i++) {
	button = document.getElementById("aproach_button_"+ i);
	console.log("i",i);
	button.innerHTML = "";
	button.setAttribute("title", "");
	button.setAttribute('style', 'width: 300px; height: 30px; background: #ebebeb; border-width: thin; border-radius: 4px; border-color: gray' );
	button.disabled = true;
    }
    approaches.forEach(function (item, index) {
	button = document.getElementById("aproach_button_"+ index);
	button.innerHTML = item.title;
	button.setAttribute("title", item.description);
	button.onclick = function() {activate_aproach(item, index)};
	button.disabled = false;
    });
    console.log(approaches[0].title);
    activate_aproach(approaches[0], 0);
    remainingDiv = document.getElementById("draggable-list");
    var fc = remainingDiv.firstChild;

    while( fc ) {
	remainingDiv.removeChild( fc );
	fc = remainingDiv.firstChild;
    }
    fill_remaining();
}



pon_leyenda = function () {
    console.log("aaaaa");
    en_cual_vamos = 0;
    console.log("vamos", en_cual_vamos);
    for (var i = 0; i < 10; i++) {
	var button = document.createElement("BUTTON");
	button.innerHTML = "";
	button.setAttribute("id", "aproach_button_"+ i);
	button.setAttribute("title", "");
	button.setAttribute('style', 'width: 300px; height: 30px; background: #ebebeb; border-width: thin; border-radius: 4px; border-color: gray' );
	button.disabled = true;
	window.leyendaDiv.appendChild(button);
    }

    var button_next = document.createElement("BUTTON");
    button_next.innerHTML = "Next"
    button_next.onclick = function() {siguientes_10()};
    button_next.setAttribute("id", "button_next");
    button_next.setAttribute("title", "Siguientes 10");
    button_next.setAttribute('style', 'width: 50px; height: 30px; background: #ebebeb; border-width: thin; border-radius: 4px; border-color: gray' );
    nextDiv = document.getElementById("controls_div");
    nextDiv.appendChild(button_next);
    remainingDiv = document.getElementById("remaining_div");


    fill_remaining();


    //highlight_aproach(0);
}



