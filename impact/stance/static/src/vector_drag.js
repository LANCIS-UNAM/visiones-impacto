///////////////////////////////////////////// las flechas
function Arrow (mouseDownPoint) {
    this.start = new Point(400, 400);
    this.headLength = 20;
    this.tailLength = 9;
    this.headAngle = 35;
    this.tailAngle = 110
}

Arrow.prototype.draw = function (point, color) {
    var end = point;
    var arrowVec = this.start.subtract(end);
    // parameterize {headLength: 20, tailLength: 6, headAngle: 35, tailAngle: 110}
    // construct the arrow
    var arrowHead = arrowVec.normalize(this.headLength);
    var arrowTail = arrowHead.normalize(this.tailLength);
    var p3 = end;                  // arrow point
    var p2 = end.add(arrowHead.rotate(-this.headAngle));   // leading arrow edge angle
    var p4 = end.add(arrowHead.rotate(this.headAngle));    // ditto, other side
    var p1 = p2.add(arrowTail.rotate(this.tailAngle));     // trailing arrow edge angle
    var p5 = p4.add(arrowTail.rotate(-this.tailAngle));    // ditto
    // specify all but the last segment, closed does that
    this.path = new paper.Path(this.start, p1, p2, p3, p4, p5);
    this.path.closed = true;
    this.path.strokeWidth = 1
    this.path.strokColor = color
    this.path.fillColor = color

    return this.path
}

//////////////////////////////////// las variables

var draggable_list = document.getElementById('draggable-list');



colores10 = ['#1f78b4','#b2df8a','#33a02c','#fb9a99','#e31a1c','#fdbf6f','#ff7f00','#cab2d6','#6a3d9a','#ffff99'];
var currentcolor = colores10[0];
var current_index = 0;
var current;
var leyendaDiv = document.getElementById("approaches_div");

// approaches are requested from REST API
var approaches = [];
var en_cual_vamos = 0;

/////////////////////////////// los ejes de orientaciones normativas 1 y 2
var ejex = new Path.Line(new Point(0, 400), new Point(800, 400));
var f_ejex_1 = new Path.Line(new Point(0, 400), new Point(10, 410));
var f_ejex_2 = new Path.Line(new Point(0, 400), new Point(10, 390));
var f_ejex_3 = new Path.Line(new Point(800, 400), new Point(790, 410));
var f_ejex_4 = new Path.Line(new Point(800, 400), new Point(790, 390));
f_ejex_1.strokeColor = 'black';
f_ejex_2.strokeColor = 'black';
f_ejex_3.strokeColor = 'black';
f_ejex_4.strokeColor = 'black';
ejex.strokeColor = 'black';
var ejey = new Path.Line(new Point(400, 0), new Point(400, 800));
var f_ejey_1 = new Path.Line(new Point(400, 0), new Point(410, 10));
var f_ejey_2 = new Path.Line(new Point(400, 0), new Point(390, 10));
var f_ejey_3 = new Path.Line(new Point(400, 800), new Point(410, 790));
var f_ejey_4 = new Path.Line(new Point(400, 800), new Point(390, 790));
f_ejey_1.strokeColor = 'black';
f_ejey_2.strokeColor = 'black';
f_ejey_3.strokeColor = 'black';
f_ejey_4.strokeColor = 'black';
ejey.strokeColor = 'black';

///////////////////////////////////////// los labels de los ejes
var ox1 = new PointText({
	point: new Point(750,420),
	justification: 'center',
	fontSize: 13,
	fillColor: 'black'
});
ox1.content = ox.pole1;


var ox2 = new PointText({
	point: new Point(50,420),
	justification: 'center',
	fontSize: 13,
	fillColor: 'black'
});
ox2.content = ox.pole2;


var oy1 = new PointText({
	point: new Point(390,50),
	justification: 'center',
	fontSize: 13,
	fillColor: 'black',
    rotation: 270
});
oy1.content = oy.pole1;


var oy2 = new PointText({
	point: new Point(410,750),
	justification: 'center',
	fontSize: 13,
	fillColor: 'black',
    rotation: 90
});
oy2.content = oy.pole2;

////////////////////////////////// las proyecciones sobre los ejes
var x = new Path.Circle(new Point(400, 400), 3);
x.strokeColor = 'black';
var y = new Path.Circle(new Point(400, 400), 3);
y.strokeColor = 'black';
var textx = new PointText({
	point: new Point(400,420),
	justification: 'center',
	fontSize: 10,
	fillColor: 'black'
});
var texty = new PointText({
	point: new Point(415,400),
	justification: 'center',
	fontSize: 10,
	fillColor: 'black'
});

view.onMouseDown = function (e) {
    approaches.forEach(function (aproach, i) {
        if (aproach.arrow.path){
            distance = e.point.getDistance(aproach.arrow.path.getNearestPoint(e.point));
        }else{
            distance = 100;
        }
        if (distance<5){
            current = aproach.arrow.path;
            currentcolor = current.strokColor;
            current_index = i;
            highlight_aproach(i);
        }
    });
}

view.onMouseDrag = function (e) {
    largo = approaches.length;
    console.log(e.point.x, e.point.y);
    if (current) current.remove()
    x.position.x = e.point.x;
    y.position.y = e.point.y;
    x.position.x = Math.min(x.position.x, 800)
    x.position.x = Math.max(x.position.x, 0)
    y.position.y = Math.min(y.position.y, 800)
    y.position.y = Math.max(y.position.y, 0)
    current = approaches[current_index].arrow.draw(new Point(x.position.x, y.position.y), currentcolor);
    textx.position.x = x.position.x
    textx.content = Math.round((x.position.x-400)/400 * 10) / 10;
    texty.position.y = y.position.y
    texty.content = Math.round((400 - y.position.y)/400 * 10) / 10;
    approaches[current_index].scores.orientacion1 = textx.content;
    approaches[current_index].scores.orientacion2 = texty.content;
}

view.onMouseUp = function (e) {
    approach = approaches[current_index].url;
    ox_mag = approaches[current_index].scores.orientacion1;
    post_projection(approach,
		    ox.url,
		    ox_mag)

    oy_mag = approaches[current_index].scores.orientacion2;
    post_projection(approach,
		    oy.url,
		    oy_mag)
    
}

highlight_aproach = function (index) {
    approaches.forEach(function (item, indx) {
        cada_boton = document.getElementById("aproach_button_"+ indx);
        cada_boton.style.fontWeight = "500";
        if (item.scores.orientacion1 == "0" && item.scores.orientacion2 == "0"){
            cada_boton.style.borderColor = "gray";
            cada_boton.style.borderWidth = "thin";
        }else{
            cada_boton.style.borderColor = item.color;
            cada_boton.style.borderWidth = "medium";
        }
        
        cada_boton.style.marginLeft = "0px"; 
        cada_boton.style.borderRadius = "4px";
    });
    el_boton = document.getElementById("aproach_button_"+ index);
    el_boton.style.fontWeight = "900";
    el_boton.style.borderColor = currentcolor;
    el_boton.style.borderWidth = "thick";
    el_boton.style.borderRadius = "4px";
    el_boton.style.marginLeft = "20px";
}

activate_aproach = function (aproach, su_index) {
    console.log(ox.url,oy.url,aproach.url);
    console.log("activando", aproach.title);
    current_index = su_index;
    currentcolor = aproach.color;
    if (proj2canvas(aproach)){
        canvas_x, canvas_y = proj2canvas(aproach);
        console.log("pondria la flecha en", canvas_x, canvas_y);
        aproach.arrow.draw(new Point(canvas_x, canvas_y), currentcolor);
    }
    current = aproach.arrow.path;
    highlight_aproach(su_index);
}

quita_flechas = function (){
    approaches.forEach(function (item, index) {
        if (item.arrow.path){
            item.arrow.path.remove();
        }
    });
}

siguientes_10 = function (){
    quita_flechas();
    // if (all_approaches.length - en_cual_vamos < 10){
    //     approaches = all_approaches.slice(en_cual_vamos);
    // }else{
    //     approaches = all_approaches.slice(en_cual_vamos, en_cual_vamos + 10); 
    // }
    approaches = remaining.slice(0, 10);
    remaining = remaining.slice(approaches.length);
    en_cual_vamos = en_cual_vamos + approaches.length;
    console.log("vamos en el ", en_cual_vamos);
    approaches.forEach(function (item, index) {
        item.color = colores10[index];
        item.scores = {orientacion1: 0, orientacion2: 0, orientacion3: 0, orientacion4: 0};
        item.arrow = new Arrow(new Point(405,405),item.color);
    });
    console.log("son",approaches.length,"approaches")
    for (var i = 0; i < 10; i++) {
        button = document.getElementById("aproach_button_"+ i);
        console.log("i",i);
        button.innerHTML = "";
        button.setAttribute("title", "");
        button.setAttribute('style', 'width: 300px; height: 30px; background: #ebebeb; border-width: thin; border-radius: 4px; border-color: gray' );
        button.disabled = true; 
    }
    approaches.forEach(function (item, index) {
        button = document.getElementById("aproach_button_"+ index);
        button.innerHTML = item.title;
        button.setAttribute("title", item.description);
        button.onclick = function() {activate_aproach(item, index)};
        button.disabled = false; 
    });
    console.log(approaches[0].title);
    activate_aproach(approaches[0], 0);
    remainingDiv = document.getElementById("draggable-list");
    var fc = remainingDiv.firstChild;

    while( fc ) {
        remainingDiv.removeChild( fc );
        fc = remainingDiv.firstChild;
    }
    fill_remaining();
}



pon_leyenda = function () {
    console.log("aaaaa");
    en_cual_vamos = 0;
    console.log("vamos", en_cual_vamos);
    for (var i = 0; i < 10; i++) {
        var button = document.createElement("BUTTON");
        button.innerHTML = "";
        button.setAttribute("id", "aproach_button_"+ i);
        button.setAttribute("title", "");
        button.setAttribute('style', 'width: 300px; height: 30px; background: #ebebeb; border-width: thin; border-radius: 4px; border-color: gray' );
        button.disabled = true;
        leyendaDiv.appendChild(button); 
    }

    var button_next = document.createElement("BUTTON");
    button_next.innerHTML = "Next"
    button_next.onclick = function() {siguientes_10()};
    button_next.setAttribute("id", "button_next");
    button_next.setAttribute("title", "Siguientes 10");
    button_next.setAttribute('style', 'width: 50px; height: 30px; background: #ebebeb; border-width: thin; border-radius: 4px; border-color: gray' );
    nextDiv = document.getElementById("controls_div");
    nextDiv.appendChild(button_next);
    remainingDiv = document.getElementById("remaining_div");

    
    fill_remaining();

    
    //highlight_aproach(0);
}   
window.onload = pon_leyenda();
