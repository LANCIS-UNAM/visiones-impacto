from stance import models
import numpy as np
import pandas as pd


def assay_df(queryset):
    """
    returns a dataframe from a queryset on the Assay class
    a dataframe suitable for rendering as parallel coordinates
    coordinates are approaches
    e.g.
    user   appr0     appr1      appr2       appr3


    """
    
    reactions = {}
    for assay in queryset:

        if assay.reagent.negative:
            reaction = -1 * assay.reaction
        else:
            reaction = assay.reaction
        
        if assay.user in reactions:                
            if assay.reagent.approach in reactions[assay.user]:
                reactions[assay.user][assay.reagent.approach].append(reaction)
            else:
                reactions[assay.user][assay.reagent.approach] = [reaction, ]
        else:
            reactions[assay.user] = {assay.reagent.approach: [reaction, ]}


    mean_reaction = {user: {appr: np.mean(reactions[user][appr])
                            for appr in reactions[user]}
                     for user in reactions}

    data = []
    for user in mean_reaction:
        row = {'user': user}
        row.update({appr: mean_reaction[user][appr]
                    for appr in mean_reaction[user]})
        data.append(row)
            
    return pd.DataFrame.from_records(data)
