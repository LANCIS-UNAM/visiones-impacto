from django.apps import AppConfig


class StanceConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'stance'
    verbose_name = 'Political Stance Reflection'
