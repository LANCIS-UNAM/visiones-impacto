from django.contrib import admin
from reversion.admin import VersionAdmin
from . import models
from .forms import ApproachForm, ReagentForm, ProjectionForm

from django.contrib.contenttypes.admin import GenericStackedInline, GenericTabularInline




class MyApproach(object):
    exclude = ('proponent', 'reviewer')

    def save_model(self, request, obj, form, change):

        if request.user.groups.filter(name='reviewers').count() == 1:
            user_is_reviewer = True
        else:
            user_is_reviewer = False
            
        if obj.status == 'v' or obj.status == 'r':
            # in reviewers group, change status            
            if user_is_reviewer:
                obj.reviewer = request.user
            else:
                obj.status = 'p'

        if not obj.id:
            # new proposal
            obj.status = 'p'
            obj.proponent = request.user
        else:
            # modified proposal
            if not user_is_reviewer:
                obj.status = 'p'
            
        obj.save()

    def has_change_permission(self, request, obj=None):
        if obj is not None:
            if request.user.is_superuser is True:
                return True
            if obj.author == request.user:
                return True
        return False

    def has_delete_permission(self, request, obj=None):
        if obj is not None:
            if request.user.is_superuser is True:
                return True
            if obj.author == request.user:
                return True
        return False



class ReagentInline(admin.TabularInline):
    model = models.Reagent
    extra = 1

class GenericTagInline(GenericTabularInline):
    model = models.TaggedItem
    extra = 0

@admin.register(models.Reference)
class ReferenceAdmin(VersionAdmin):
    inlines = [GenericTagInline]


@admin.register(models.Approach)
class ApproachAdmin(MyApproach, VersionAdmin):
    form = ApproachForm
    search_fields = ['title', 'description', ]
    list_display = ('title', 'year_start', 'country', 'description', 'proponent', 'status', 'reviewer')
    list_filter = ['proponent', 'status', 'reviewer']
    inlines = [ReagentInline, GenericTagInline]
    readonly_fields = ['proponent', 'reviewer', ]


@admin.register(models.Reagent)
class ReagentAdmin(VersionAdmin):
    list_display = ('assertion', 'approach')
    form = ReagentForm
    inlines = [GenericTagInline]


@admin.register(models.Assay)
class ReferenceAdmin(admin.ModelAdmin):
    list_display = ('user', 'reaction', 'reagent')


@admin.register(models.Projection)
class ProjectionAdmin(VersionAdmin):
    list_display = ("orientation", "magnitude", "approach", "author")
    form = ProjectionForm

    def save_model(self, request, obj, form, change):
        obj.author = request.user
        super().save_model(request, obj, form, change)


@admin.register(models.NormativeOrientation)
class OrientationAdmin(VersionAdmin):
    list_display = ('title', )

admin.site.site_header = "Introspector"
