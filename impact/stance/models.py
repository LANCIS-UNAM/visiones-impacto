from django.db import models
from django.contrib.auth.models import User, Group
from django.utils.translation import gettext_lazy as _

from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from countries_plus.models import Country


class TaggedItem(models.Model):
    tag = models.SlugField()
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, related_name='types')
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    def __str__(self):
        return self.tag


class Reference(models.Model):
    title = models.CharField(max_length=255,
                             verbose_name=_("title"),
                             help_text=_("title"))
    citation = models.TextField(verbose_name=_("citation"),
                                   help_text=_("citation, APA style"))

    bibtex = models.TextField(null=True, blank=True)

    url = models.URLField(null=True, blank=True,
                          help_text=_('preferably DOI but others are fine if not available'))

    year = models.IntegerField(default=1900,
                               help_text=_('year of publication'))

    tags = GenericRelation(TaggedItem,
                           related_query_name='reference')

    class Meta:
        ordering = ['title']

    def __str__(self):
        return self.title



class Approach(models.Model):
    title = models.CharField(max_length=255,
                             verbose_name=_("title"),
                             help_text=_("title"))
    description = models.TextField(verbose_name=_("description"),
                                   help_text=_("description"))

    discourse = models.TextField(verbose_name=_("socio-political discourse"),
                                 help_text=_("socio-political discourse"),
                                 null=True, blank=True,                                 
                                 default="")

    strong = models. BooleanField(default=False,
                                  help_text=_('Is this approach soft or strong?'))

    bibliography = models.ManyToManyField(Reference, blank=True)

    year_start = models.IntegerField(help_text="Start year", default=1900)
    year_end = models.IntegerField(help_text="End year", null=True, blank=True)

    
    country = models.ForeignKey(Country, on_delete=models.CASCADE)

    founder = models.CharField(max_length=255,
                               help_text=_("The person or comunity that started this approach"),
                               null=True, blank=True)
                               

    tags = GenericRelation(TaggedItem,
                           related_query_name='approach')

    proponent = models.ForeignKey(User,
                                  on_delete=models.CASCADE,
                                  verbose_name=_("proponent"),
                                  related_name=_('proposed'),
                                  help_text=_("person that submits an approach for our platform"))

    reviewer = models.ForeignKey(User,
                                 on_delete=models.CASCADE,
                                 null=True, blank=True,                                 
                                 verbose_name=_("reviewer"),
                                 help_text=_("person that reviews a proposed approach"))

    status = models.CharField(
        max_length=2,
        default='p',
        choices=(
            ('p', 'p'),  # proposal
            ('v', 'v'),  # validated
            ('r', 'r'),  # rejected
        ))


    class Meta:
        verbose_name_plural = "Approaches"

    def __str__(self):
        return self.title


class Reagent(models.Model):
    assertion = models.TextField(verbose_name=_("assertion"),
                                 help_text=_("description"))

    negative = models. BooleanField(default=False,
                                    help_text=_('reactions against this reagent should be interpreted as supportive of approach'))

    approach = models.ForeignKey(Approach,
                                 on_delete=models.CASCADE,
                                 verbose_name=_("approach"),
                                 help_text=_("approach"))

    tags = GenericRelation(TaggedItem,
                           related_query_name='reagent')

    def __str__(self):
        return self.assertion




class Assay(models.Model):
    reagent = models.ForeignKey(Reagent,
                                on_delete=models.CASCADE)
    reaction = models.SmallIntegerField(default=0,
                                        verbose_name=_("reaction"),
                                        help_text=_("reaction"))
    user = models.ForeignKey(User,
                             on_delete=models.CASCADE)


    class Meta:
        unique_together = ('reagent', 'user')


class NormativeOrientation(models.Model):
    """
    the ascertainment of one's true position, as in a novel situation,
    with respect to attitudes, judgments, etc.
    """
    title = models.CharField(max_length=255,
                             verbose_name=_("title"),
                             help_text=_("title"))

    pole1 = models.CharField(max_length=255,
                             verbose_name=_("pole 1"),
                             help_text=_("one extreme"))
    pole2 = models.CharField(max_length=255,
                             verbose_name=_("pole 2"),
                             help_text=_("the other extreme"))


    description = models.TextField(verbose_name=_("description"),
                                   help_text=_("description"))
    bibliography = models.ManyToManyField(Reference, blank=True)

    def __str__(self):
        return self.title



class Projection(models.Model):
    approach = models.ForeignKey(Approach, on_delete=models.CASCADE)
    orientation = models.ForeignKey(NormativeOrientation,
                                              on_delete=models.CASCADE)
    magnitude = models.DecimalField(verbose_name=_("magnitude"),
                                    help_text=_("value [-1,1] that represents the projection of a dimension on an approach"),
                                    max_digits=4,
                                    decimal_places=3)

    author = models.ForeignKey(User,
                               on_delete=models.CASCADE,
                               verbose_name=_("author"),
                               help_text=_("author"))

    # TODO: llave triple para que no haya magnitudes duplicadas en tuplas de approach-orientation

    def __str__(self):
        return "%s %s" % (self.approach,
                          self.orientation)
