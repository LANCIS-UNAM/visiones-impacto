from django.shortcuts import render
from dal import autocomplete
from stance import models
from django.views import View
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy, reverse
import random
from stance import forms
from django.views import generic
from django.template.loader import render_to_string

import tempfile
import codecs
import subprocess
from time import sleep
from os import path, chdir
from django.http import HttpResponse

import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.backends.backend_agg import FigureCanvasAgg
from pandas.plotting import parallel_coordinates

from stance.wrangle import assay_df

from rest_framework import viewsets
from rest_framework import permissions
from stance import serializers
from django.conf import settings
from pathlib import Path
import pypandoc


class ReferenceAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return models.Reference.objects.none()

        qs = models.Reference.objects.all()

        if self.q:
            qs = qs.filter(title__istartswith=self.q)

        return qs




class ApproachAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return models.Approach.objects.none()

        qs = models.Approach.objects.all()

        if self.q:
            qs = qs.filter(title__istartswith=self.q)

        return qs



class AssayView(View):
    form_class = forms.AssayForm
    template_name = 'stance/random_assay.html'

    def get(self, request, *args, **kwargs):
        all = set(models.Reagent.objects.all())
        answered = set([a.reagent for a in
                        models.Assay.objects.filter(user=request.user)])
        unanswered = list(all - answered)

        if unanswered:
            reagent = random.choice(unanswered)
            form = self.form_class(initial={'reagent_id': reagent.id})
            return render(request,
                          self.template_name,
                          {'reagent': reagent,
                           'form': form})
        else:
            return HttpResponseRedirect('/admin/stance/assay/')


    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():

            models.Assay(
                reagent=models.Reagent.objects.get(pk=request.POST['reagent_id']),
                reaction=request.POST['reaction'],
                user=self.request.user).save()



        return HttpResponseRedirect(reverse('assay'))



class ApproachListView(generic.ListView):
    model = models.Approach

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context['approach_list'] = models.Approach.objects.all()
        context['reagent_list'] = models.Reagent.objects.all()

        return context



def buildPDF(request):

    module_dir = path.dirname(__file__)

    tex_path = path.join(module_dir, 'pdfs', 'approaches.tex')
    with codecs.open (tex_path, "w", "utf-8") as tex_file:
        output = render_to_string('stance/approaches.tex',
                                  {'approach_list' : models.Approach.objects.all()})
        tex_file.write(output)

    sleep(1)
    chdir(path.join(module_dir, 'pdfs'))
    subprocess.run(["xelatex", "-interaction=nonstopmode", tex_path],
                   stdin=subprocess.DEVNULL,
                   stdout=subprocess.DEVNULL,
                   stderr=subprocess.DEVNULL)

    pdf_path = tex_path.replace(".tex",".pdf")

    with open(pdf_path, 'rb') as pdf:
        response = HttpResponse(pdf.read(),content_type='application/pdf')
        response['Content-Disposition'] = 'filename=approaches.pdf'
        return response

def buildODT(request):

    outdir = Path(settings.MEDIA_ROOT).joinpath("odt")
    outdir.mkdir(exist_ok=True, parents=True)
    
    
    md_path = outdir.joinpath("approaches.md")


    with open(md_path, "w") as md_file:
        output = render_to_string('stance/approaches.md',
                                  {'approach_list' : models.Approach.objects.all()})
        md_file.write(output)


    odt_path = outdir.joinpath("approaches.odt")
    pypandoc.convert_file(str(md_path), 'odt', outputfile=str(odt_path))


    with open(odt_path, 'rb') as odt:
        response = HttpResponse(odt.read(),content_type='application/odt')
        response['Content-Disposition'] = 'filename=approaches.odt'
        return response


def stance_pc(request):

    df = assay_df(models.Assay.objects.all())

    fig, ax = plt.subplots()
    parallel_coordinates(df, 'user', ax=ax)
    plt.xticks(rotation=90)
    fig.tight_layout()
    
    response = HttpResponse(content_type = 'image/png')
    canvas = FigureCanvasAgg(fig)
    canvas.print_png(response)
    return response




class ProjectionView(View):
    form_class = forms.AssayForm
    template_name = 'stance/projection.html'
    
    def get(self, request, *args, **kwargs):
        
        ox = models.NormativeOrientation.objects.get(pk=kwargs['ox_id'])
        oy = models.NormativeOrientation.objects.get(pk=kwargs['oy_id'])
        
        return render(request,
                      self.template_name,
                      {'ox': ox,
                       'oy': oy}
                      )




class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = models.User.objects.all().order_by('-date_joined')
    serializer_class = serializers.UserSerializer
    permission_classes = [permissions.IsAuthenticated]


class ApproachViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = models.Approach.objects.all()
    serializer_class = serializers.ApproachSerializer
    permission_classes = [permissions.IsAuthenticated]
    

class NormativeOrientationViewSet(viewsets.ModelViewSet):
    queryset = models.NormativeOrientation.objects.all()
    serializer_class = serializers.NormativeOrientationSerializer
    permission_classes = [permissions.IsAuthenticated]


class ProjectionViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.ProjectionSerializser
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        return models.Projection.objects.filter(author=self.request.user)

