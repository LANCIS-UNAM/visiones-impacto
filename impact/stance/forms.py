from dal import autocomplete
from django import forms
from stance import models
from django.forms import HiddenInput


class ApproachForm(forms.ModelForm):

    class Meta:
        model = models.Approach
        fields = ('__all__')
        widgets = {
            'bibliography': autocomplete.ModelSelect2Multiple(url='reference-autocomplete')
        }



class ReagentForm(forms.ModelForm):

    class Meta:
        model = models.Reagent
        fields = ('__all__')
        widgets = {
            'approach': autocomplete.ModelSelect2(url='approach-autocomplete')
        }



class ProjectionForm(forms.ModelForm):

    class Meta:
        model = models.Projection
        fields = ('approach', 'orientation', 'magnitude')
        widgets = {
            'approach': autocomplete.ModelSelect2(url='approach-autocomplete')
        }



class AssayForm(forms.Form):

    reaction = forms.IntegerField(widget=HiddenInput(),
                                  min_value=-2,
                                  max_value=2)

    reagent_id = forms.IntegerField(widget=HiddenInput())
