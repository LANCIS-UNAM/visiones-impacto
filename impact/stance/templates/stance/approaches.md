

# Approach List

{% for approach in approach_list %}
## {{approach.title}} 
  
{{approach.description}}
  

  {% if approach.bibliography.all %}
### Bibliografía 
  
  {% for bib in approach.bibliography.all %}
 -  {{ bib.citation }} 
  {% endfor %}
 
  {% endif %}

### Afirmaciones
 
  {% for reagent in approach.reagent_set.all %}
    {% if reagent.approach.id == approach.id %}
      {% if not reagent.negative  %} 
 -  {{ reagent.assertion }}  
      {% endif %}
    {% endif %}
  {% endfor %}
  
### Negaciones 
  
  {% for reagent in approach.reagent_set.all %}
    {% if reagent.approach.id == approach.id %}
      {% if reagent.negative  %} 
 -  {{ reagent.assertion }}  
      {% endif %}
    {% endif %}
  {% endfor %}
{% endfor %}