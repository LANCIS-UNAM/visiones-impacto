# Generated by Django 3.2.3 on 2021-05-19 01:46

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('toc', '0003_auto_20210512_1827'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='problem',
            name='causes',
        ),
        migrations.CreateModel(
            name='Goal',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200)),
                ('description', models.TextField()),
                ('problem', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='toc.problem')),
                ('project', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='toc.project')),
            ],
        ),
        migrations.CreateModel(
            name='CausalEdge',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('source', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='causes', to='toc.problem')),
                ('target', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='caused_by', to='toc.problem')),
            ],
        ),
        migrations.CreateModel(
            name='BibRef',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ref', models.TextField()),
                ('causal_edge', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='toc.causaledge')),
            ],
        ),
    ]
