from django.db import models
from stance.models import TaggedItem, Reference
from django.utils.translation import gettext_lazy as _
from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType



class Project(models.Model):
    title = models.CharField(max_length=200)
    staff_size = models.IntegerField(default=1)

    # shape, usar geodjango
    beneficiaries = models.TextField()
    problem_space = models.TextField()

    tags = GenericRelation(TaggedItem,
                           related_query_name='reference')
    bibliography = models.ManyToManyField(Reference, blank=True)

    def __str__(self):
        return self.title

    

class Status(models.Model):
    title = models.CharField(max_length=200)
    goal = models.BooleanField(default=False)

    bibliography = models.ManyToManyField(Reference, blank=True)
    projects = models.ManyToManyField(Project, blank=True)

    class Meta:
        verbose_name_plural = "Status"

    def __str__(self):
        return self.title


class Intervention(models.Model):
    title = models.CharField(max_length=200)    
    source = models.ForeignKey(Status, on_delete=models.CASCADE, related_name="causes")
    target = models.ForeignKey(Status, on_delete=models.CASCADE, related_name="caused_by")
    description = models.TextField(verbose_name=_("description"),
                                   help_text=_("description"),
                                   blank=True)
    bibliography = models.ManyToManyField(Reference, blank=True)

    projects = models.ManyToManyField(Project, blank=True)
    def __str__(self):
        return self.source.title+"_"+self.target.title



class TaggedItem(models.Model):
    tag = models.SlugField()
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    def __str__(self):
        return self.tag


class Reference(models.Model):
    title = models.CharField(max_length=255,
                             verbose_name=_("title"),
                             help_text=_("title"))
    citation = models.TextField(verbose_name=_("citation"),
                                   help_text=_("citation"))

    bibtex = models.TextField(null=True, blank=True)

    url = models.URLField(null=True, blank=True)

    tags = GenericRelation(TaggedItem,
                           related_query_name='reference')
    
    class Meta:
        ordering = ['title']

    def __str__(self):
        return self.title
    
