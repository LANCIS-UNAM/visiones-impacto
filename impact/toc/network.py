import networkx as nx
from toc import models


def toc_digraph(intervention_qs):
    G = nx.DiGraph()

    for i in intervention_qs:
        G.add_edge(i.source,
                   i.target,
                   intervention=i)

    return G
