from django.shortcuts import render
from toc import models
from toc.network import toc_digraph
import matplotlib.pyplot as plt
from matplotlib.backends.backend_agg import FigureCanvasAgg
import networkx as nx
from django.http import HttpResponse


def toc_digraph_view(request):

    # TODO: maybe graph for project? maybe use django-filter?
    g = toc_digraph(models.Intervention.objects.all())

    fig, ax = plt.subplots()

    # TODO: use pydot
    nx.draw(g, with_labels=True,
            font_size=9,
            node_color='firebrick')    

    response = HttpResponse(content_type = 'image/png')
    canvas = FigureCanvasAgg(fig)
    canvas.print_png(response)
    return response

