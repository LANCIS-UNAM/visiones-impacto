from django.contrib import admin
from . import models

# Register your models here

@admin.register(models.Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = ('title',)


@admin.register(models.Status)
class StatusAdmin(admin.ModelAdmin):
    list_display = ('title', 'goal')

@admin.register(models.Intervention)
class InterventionAdmin(admin.ModelAdmin):
    list_display = ('title', 'source', 'target')

    
