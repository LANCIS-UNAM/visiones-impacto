from kivy.app import App
from kivy.core.window import Window
from kivy.graphics import Color, Line, Rectangle, Ellipse
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label

Window.set_system_cursor('crosshair')

DRAG_START = ()
DRAG_END = ()
DRAGGING = False


class Widget(BoxLayout):
    def __init__(self, **kwargs):
        super(Widget, self).__init__(**kwargs)
        Window.bind(mouse_pos=self.mouse_pos)
        self.bind(pos=self.draw)
        self.bind(size=self.draw)
        self.layout1 = BoxLayout(opacity=1)
        self.add_widget(self.layout1)
        self.vpos = (self.center_x, self.center_y)
        print(self.layout1.width)
        print(self.layout1.height)
        print(self.width)
        print(self.height)


    def draw(self, *args):
        self.layout1.canvas.clear()
        with self.canvas.before:
            Color(0.6, 0.6, 0.6, 1)
            self.bg = Rectangle(pos=self.pos, size=self.size)
            self.draw_scale()
            print(self.vpos)

    def draw_scale(self):
        Color(0, 0, 0, 0.2)
        Line(
                points=[self.center_x, 0, self.center_x, self.center_y * 2],
                width=1.4)
        Color(0, 0, 0, 0.2)
        Line(
                points=[0, self.center_y, self.center_x * 2, self.center_y],
                width=1.4)
 

    def drawLine(self, mPos):
        self.canvas.clear()
        with self.canvas:
            Color(0, 0, 0, 1)
            Line(
                points=[self.center_x, self.center_y, mPos[0], mPos[1]],
                width=1.4)
            self.la_x = '%0.2f' % ((self.vpos[0] - self.center_x)*2 / self.size[0])
            self.la_y = '%0.2f' %((self.vpos[1] - self.center_y)*2 / self.size[1])
            Color(1, 0, 0)
            Ellipse(pos=(self.vpos[0]-5, self.center_y-5), size=(10, 10))
            dy_x = -63
            dx_x = -50
            Label(pos=(self.vpos[0] + dx_x, self.center_y + dy_x), text=self.la_x)
            Ellipse(pos=(self.center_x-5, self.vpos[1]-5), size=(10, 10))
            dy_y = -50
            dx_y = -25
            Label(pos=(self.center_x + dx_y, self.vpos[1] + dy_y), text=self.la_y)


        
        def on_motion(self, instance, new_size):
            #self.drawLine()
            print('aguas', self.size)
            


        self.vpos = mPos


    def mouse_pos(self, window, pos):
        if DRAGGING == True:
            self.drawLine(pos)

    def on_touch_down(self, event):
        global DRAGGING, DRAG_START
        DRAGGING = True
        DRAG_START = event.pos

    def on_touch_up(self, event):
        global DRAGGING, DRAG_END
        DRAGGING = False
        DRAG_END = event.pos


class App(App):
    title = "Kivy Click Drag Line"

    def build(self):
        return Widget()


if __name__ == "__main__":
    App().run()
